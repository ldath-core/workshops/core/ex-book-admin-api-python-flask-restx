# ex-book-admin-api-flask-restx

## Requirements

This Project requires those tools for the development:

- **Brew** - optional
- **Docker Engine** - required
- **Docker Compose** - required
- **Ansible** - required
- **Helm** - optional
- **K3d** - optional
- **Trivy** - optional
- **Grype** - optional

and `Python` installed and configured

## Flask frameworks comparison

Flask RESTplus started as a fork of Flask-RESTful.
Flask RESTx is a fork of Flask REST plus that is not currently developed.
The main difference between Flask-RESTx and Flask-RESTful is that Flask-RESTx supports auto-generated Swagger documentation.

## Directory structure

The project source code is included in app directory. The main script is called app.py which initializes app and read
configuration included in config.py file. Config.py reads database access data from generated (or created manually)
yaml file that path is read from CFG_FILE environment variable. If the variable is not defined application searches for 
<REPO_DIR>/secrets/local.env.yaml file. The endpoints definition is put in the controller package that
contains v1 blueprint definition. The blueprint contains two endpoints namespaces: /admin and /health. API is mainly
described in admin_controller.py and health controller.py files with use of decorators provided by Flask RESTx. JSON
bodies data structures are presented as classes located in dto package. A connection between controllers and DTOs is
done admin_api.py and health_api.py files. Fields description is registered here as API models. The whole endpoint's business logic is delegated handler package. The handler package uses the following packages:

- validator - for input body validation logic
- model - as a description of the database schema structure
- converter - for conversion from database entities object instances into DTO instances that are possible to be marshaled to JSON in controllers  

## Configuration procedure

Prepare configuration:

    ./local_configure.sh

Because this is only for presentation purposes password is: `ThisIsExamplePassword4U`

By the end of running this, you should have files in the `secret` folder which will be used by all the Development
methods.
