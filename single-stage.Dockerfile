FROM python:3.10-alpine

COPY app /app
COPY secrets /secrets

WORKDIR /app

RUN pip3 install -r requirements.txt

EXPOSE 8080

ENV CFG_FILE=/secrets/local.env.yaml

CMD [ "flask", "run", "--host", "0.0.0.0", "--port", "8080" ]
