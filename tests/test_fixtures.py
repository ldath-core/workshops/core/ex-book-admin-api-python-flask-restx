from pytest import fixture

from app.app import create_app
from app.handler.database import get_db


@fixture()
def app():
    app = create_app()

    with app.app_context():
        db = get_db()

    test_data = ({
        'email': 'admin%d@gmail.com' % id,
        'passwordHash': 'password' + str(id),
        'firstName': 'firstname' + str(id),
        'lastName': 'lastname' + str(id),
        'version': 1
    } for id in range(100))

    db.drop_collection('admins')
    db.admins.insert_many(test_data)

    yield app


@fixture()
def client(app):
    return app.test_client()


@fixture()
def runner(app):
    return app.test_cli_runner()
