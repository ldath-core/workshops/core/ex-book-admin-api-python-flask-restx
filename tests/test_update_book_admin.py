from pytest import mark

from test_fixtures import *
from tests.test_book_admins import TestBookAdmins


@mark.usefixtures('client')
class TestUpdateBookAdmin:

    def test_should_update_book_admin_email(self, client):
        # given
        id = TestBookAdmins.get_admin_id(client)
        patch = {'email': 'updated@gmail.com'}

        # when
        response = client.put('/v1/admins/' + id, json=patch)

        # then
        TestBookAdmins.check_response(response, 202, 'admin: %s updated' % id)
        TestBookAdmins.check_response_content(
            response,
            id,
            patch['email'],
            'firstname0',
            'lastname0')

    def test_should_update_book_admin_password(self, client):
        # given
        id = TestBookAdmins.get_admin_id(client)
        patch = {'password': 'updated'}

        # when
        response = client.put('/v1/admins/' + id, json=patch)

        # then
        TestBookAdmins.check_response(response, 202, 'admin: %s updated' % id)
        TestBookAdmins.check_response_content(
            response,
            id,
            'admin0@gmail.com',
            'firstname0',
            'lastname0')

    def test_should_update_book_admin_first_name(self, client):
        # given
        id = TestBookAdmins.get_admin_id(client)
        patch = {'firstName': 'Updated'}

        # when
        response = client.put('/v1/admins/' + id, json=patch)

        # then
        TestBookAdmins.check_response(response, 202, 'admin: %s updated' % id)
        TestBookAdmins.check_response_content(
            response,
            id,
            'admin0@gmail.com',
            patch['firstName'],
            'lastname0')

    def test_should_update_book_admin_last_name(self, client):
        # given
        id = TestBookAdmins.get_admin_id(client)
        patch = {'lastName': 'Updated'}

        # when
        response = client.put('/v1/admins/' + id, json=patch)

        # then
        TestBookAdmins.check_response(response, 202, 'admin: %s updated' % id)
        TestBookAdmins.check_response_content(
            response,
            id,
            'admin0@gmail.com',
            'firstname0',
            patch['lastName'])

    def test_should_update_book_admin_version(self, client):
        # given
        id = TestBookAdmins.get_admin_id(client)
        patch = {'version': 1}

        # when
        response = client.put('/v1/admins/' + id, json=patch)

        # then
        TestBookAdmins.check_response(response, 202, 'admin: %s updated' % id)
        TestBookAdmins.check_response_content(
            response,
            id,
            'admin0@gmail.com',
            'firstname0',
            'lastname0',
            patch['version'])

    def test_should_reject_request_with_invalid_value_type(self, client):
        # given
        id = TestBookAdmins.get_admin_id(client)
        patch = {'email': 1}

        # when
        response = client.put('/v1/admins/' + str(id), json=patch)

        # then
        TestBookAdmins.check_error_response(response, 400, 'email must be string')

    def test_should_reject_request_with_empty_field_value(self, client):
        # given
        id = TestBookAdmins.get_admin_id(client)
        patch = {'password': ''}

        # when
        response = client.put('/v1/admins/' + str(id), json=patch)

        # then
        TestBookAdmins.check_error_response(response, 400, 'password must not be empty')

    def test_should_reject_request_with_blank_field_value(self, client):
        # given
        id = TestBookAdmins.get_admin_id(client)
        patch = {'firstName': ' '}

        # when
        response = client.put('/v1/admins/' + str(id), json=patch)

        # then
        TestBookAdmins.check_error_response(response, 400, 'firstName must not be blank')

    def test_should_reject_request_with_unexpected_field(self, client):
        # given
        id = TestBookAdmins.get_admin_id(client)
        patch = {'unexpected': 'unexpected'}

        # when
        response = client.put('/v1/admins/' + str(id), json=patch)

        # then
        TestBookAdmins.check_error_response(
            response,
            400,
            'only keys: email, password, firstName, lastName, version are allowed')

    def test_should_return_not_found_for_invalid_id(self, client):
        # given
        id = 10
        patch = {'email': 'updated@gmail.com'}

        # when
        response = client.put('/v1/admins/' + str(id), json=patch)

        # then
        TestBookAdmins.check_error_response(response, 404, 'there is no documents with this id')

    def test_should_return_not_found_for_valid_id(self, client):
        # given
        id = '63f36b8271a01f8f5f384a07'
        patch = {'email': 'updated@gmail.com'}

        # when
        response = client.put('/v1/admins/' + id, json=patch)

        # then
        TestBookAdmins.check_error_response(response, 404, 'there is no documents with this id')
