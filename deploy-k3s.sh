#!/usr/bin/env bash
set -e
kubectx k3d-bookCluster
./build-single-stage-container.sh
k3d image import -c bookCluster ex-book-admin-python-flask-restx:latest
helm upgrade -i -f secrets/k3s-values.yaml --namespace=services k3s-ex-book-admin-api-python-flask-restx ex-book/ex-book-helm-chart --version 0.3.2 --dry-run --debug
#helm upgrade -i -f secrets/k3s-values.yaml --namespace=services k3s-ex-book-admin-api-python-flask-restx ex-book/ex-book-helm-chart --version 0.3.2 --wait
