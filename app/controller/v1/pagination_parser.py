from flask_restx import inputs
from flask_restx.reqparse import RequestParser

pagination_parser = RequestParser()
pagination_parser.add_argument(
    'skip',
    type=inputs.natural,
    required=False,
    default=0,
    help='how many records to skip')

pagination_parser.add_argument(
    'limit',
    type=inputs.positive,
    required=False,
    default=10,
    help='how many records to return')

pagination_parser.add_argument(
    'email',
    type=inputs.email(),
    required=False,
    help='filer with email')
