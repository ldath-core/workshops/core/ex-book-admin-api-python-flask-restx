from flask_restx import Resource, reqparse, marshal

from app.controller.v1.admin_api import AdminApi
from app.controller.v1.pagination_parser import pagination_parser
from app.handler.book_admin_handler import BookAdminHandler
from app.handler.book_admin_list_handler import BookAdminListHandler


@AdminApi.api.route('/')
class BookAdminListController(Resource):

    @AdminApi.api.doc(
        'Get BookAdmins',
        description='This endpoint is sending details of all book-admins')
    @AdminApi.api.expect(pagination_parser)
    @AdminApi.api.response(200, 'Success response for the book-admins request', AdminApi.book_admins_output)
    @AdminApi.api.response('default', 'Unexpected error', AdminApi.error)
    @AdminApi.api.marshal_with(AdminApi.book_admins_output)
    def get(self):
        result = BookAdminListHandler.get(pagination_parser)
        return result, result.status

    @AdminApi.api.doc(
        'Create BookAdmin',
        description='This endpoint creating new book-admin and is sending details of book-admin with assigned ID')
    @AdminApi.api.expect(AdminApi.create_book_admin_input)
    @AdminApi.api.response(201, 'Success response for the book-admin creation request', AdminApi.created_book_admin_output)
    @AdminApi.api.response('default', 'Unexpected error', AdminApi.error)
    @AdminApi.api.marshal_with(AdminApi.created_book_admin_output, code=201)
    def post(self):
        result = BookAdminListHandler.post(reqparse.request.json)
        return result, result.status


@AdminApi.api.route('/<string:id>')
class BookAdminController(Resource):

    @AdminApi.api.doc(
        'Get BookAdmin by ID',
        description='This endpoint is sending details of book-admin chosen by ID')
    @AdminApi.api.response(200, 'Success response for the book-admin request', AdminApi.book_admin_output)
    @AdminApi.api.response(404, 'Not Found', AdminApi.error)
    @AdminApi.api.response('default', 'Unexpected error', AdminApi.error)
    def get(self, id: str):
        result = BookAdminHandler.get(id)
        if result.status == 200:
            return marshal(result, AdminApi.book_admin_output), 200
        return marshal(result, AdminApi.error), result.status

    @AdminApi.api.doc(
        'Put BookAdmin by ID',
        description='This endpoint is updating book-admin')
    @AdminApi.api.expect(AdminApi.update_book_admin_input)
    @AdminApi.api.response(202, 'Success response for the book-admin request', AdminApi.book_admin_output)
    @AdminApi.api.response(404, 'Not Found', AdminApi.error)
    @AdminApi.api.response('default', 'Unexpected error', AdminApi.error)
    def put(self, id: str):
        result = BookAdminHandler.put(id, reqparse.request.json)
        if result.status == 202:
            return marshal(result, AdminApi.book_admin_output), 202
        return marshal(result, AdminApi.error), result.status

    @AdminApi.api.doc(
        'Delete BookAdmin by ID',
        description='This endpoint is deleting book-admin')
    @AdminApi.api.response(202, 'Default Success response', AdminApi.success)
    @AdminApi.api.response(404, 'Not Found', AdminApi.error)
    @AdminApi.api.response('default', 'Unexpected error', AdminApi.error)
    def delete(self, id: str):
        result = BookAdminHandler.delete(id)
        if result.status == 202:
            return marshal(result, AdminApi.success), 202
        return marshal(result, AdminApi.error), result.status
