from flask_restx import Namespace

from app.dto.v1.book_admin_object import BookAdminObject
from app.dto.v1.book_admins_output import PaginatedBookAdminsContent, BookAdminsOutput
from app.dto.v1.create_book_admin_input import CreateBookAdminInput
from app.dto.v1.create_book_admin_output import CreatedBookAdmin, CreateBookAdminOutput
from app.dto.v1.error import Error
from app.dto.v1.success import Success
from app.dto.v1.update_book_admin_input import UpdateBookAdminInput


class AdminApi:

    api = Namespace('book-admin', path='/admins', description='BookAdmin related endpoints')

    # initialize response models
    success = api.model('Success', Success.get_model())
    error = api.model('Error', Error.get_model())

    book_admin_object = api.model('BookAdminObject', BookAdminObject.get_model())

    paginated_book_admins_content = api.model('PaginatedBookAdminsContent', PaginatedBookAdminsContent.get_model(book_admin_object))
    book_admins_output = api.model('BookAdminsOutput', BookAdminsOutput.get_model(paginated_book_admins_content))

    created_book_admin = api.model('CreatedBookAdmin', CreatedBookAdmin.get_model())
    created_book_admin_output = api.model('CreateBookAdminOutput', CreateBookAdminOutput.get_model(created_book_admin))

    book_admin_output = api.model('BookAdminsOutput', BookAdminsOutput.get_model(book_admin_object))

    # initialize request models
    create_book_admin_input = api.model('CreateBookAdminInput', CreateBookAdminInput.get_model())

    update_book_admin_input = api.model('UpdateBookAdminInput', UpdateBookAdminInput.get_model())
