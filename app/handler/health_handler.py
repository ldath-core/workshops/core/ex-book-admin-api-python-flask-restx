from flask import current_app
from pymongo import MongoClient
from pymongo.errors import ServerSelectionTimeoutError

from app.dto.v1.health import Health, HealthContent


class HealthHandler:

    @staticmethod
    def get():
        mongo = True
        try:
            connection = MongoClient(current_app.config['MONGO_URI'], serverSelectionTimeoutMS=1000)
            connection.server_info()
            connection.close()
        except ServerSelectionTimeoutError:
            mongo = False

        return Health(200, 'book admin api health', HealthContent(alive=True, mongo=mongo))
