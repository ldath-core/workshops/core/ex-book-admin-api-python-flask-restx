from typing import Union

from flask_pymongo import ObjectId, InvalidId

from app.converter.converter import convert_book_admin_entity_to_dto
from app.dto.v1.book_admin_output import BookAdminsOutput
from app.dto.v1.error import Error
from app.dto.v1.success import Success
from app.handler.database import get_db
from app.validator.validator import InvalidRequestBodyError, is_book_admin_input_update_valid


class BookAdminHandler:

    @staticmethod
    def get(id: str) -> Union[BookAdminsOutput, Error]:
        db = get_db()

        try:
            object_id = ObjectId(id)
        except InvalidId:
            return Error(404, 'there is no documents with this id', [])

        admin = db.admins.find_one({'_id': object_id})

        if admin is None:
            return Error(404, 'there is no documents with this id', [])

        return BookAdminsOutput(
            200,
            'admin',
            convert_book_admin_entity_to_dto(admin))

    @staticmethod
    def put(id: str, request_body: dict) -> Union[BookAdminsOutput, Error]:
        db = get_db()

        try:
            object_id = ObjectId(id)
        except InvalidId:
            return Error(404, 'there is no documents with this id', [])

        try:
            is_book_admin_input_update_valid(request_body)
        except InvalidRequestBodyError as e:
            return BookAdminsOutput(400, str(e), None)

        admin = db.admins.find_one({'_id': object_id})
        if admin is None:
            return Error(404, 'there is no documents with this id', [])

        db.admins.update_one({'_id': object_id}, {'$set': request_body})

        return BookAdminsOutput(
            202,
            'admin: %s updated' % id,
            convert_book_admin_entity_to_dto(db.admins.find_one({'_id': object_id})))

    @staticmethod
    def delete(id: str) -> Union[Success, Error]:
        db = get_db()

        try:
            object_id = ObjectId(id)
        except InvalidId:
            return Error(404, 'there is no documents with this id', [])

        admin = db.admins.find_one({'_id': object_id})
        if admin is None:
            return Error(404, 'there is no documents with this id', [])

        db.admins.delete_one({'_id': object_id})

        return Success(202, 'admin: %s deleted' % id, '')
