from bcrypt import hashpw, gensalt
from pymongo import ASCENDING
from flask_restx.reqparse import RequestParser

from app.converter.converter import convert_book_admin_entity_to_dto
from app.dto.v1.book_admins_output import BookAdminsOutput, PaginatedBookAdminsContent
from app.dto.v1.create_book_admin_output import CreateBookAdminOutput, CreatedBookAdmin
from app.handler.database import get_db
from app.validator.validator import is_book_admin_input_valid, InvalidRequestBodyError


class BookAdminListHandler:

    @staticmethod
    def get(pagination_parser: RequestParser) -> BookAdminsOutput:
        db = get_db()

        args = pagination_parser.parse_args()
        skip = args.get('skip')
        limit = args.get('limit')
        email = args.get('email')

        count = db.admins.count_documents({})

        if email is None:
            admins = db.admins.find()
        else:
            admins = db.admins.find({'email': email})

        admins = tuple(
            convert_book_admin_entity_to_dto(admin)
            for admin in admins.sort('_id', ASCENDING).skip(skip * limit).limit(limit))

        return BookAdminsOutput(
            200,
            'admins - skip: %d; limit: %d' % (skip, limit),
            PaginatedBookAdminsContent(count, skip, limit, admins))

    @staticmethod
    def post(request_body: dict) -> CreateBookAdminOutput:
        db = get_db()

        try:
            is_book_admin_input_valid(request_body)
        except InvalidRequestBodyError as e:
            return CreateBookAdminOutput(400, str(e), None)

        request_body['passwordHash'] = hashpw(bytes(request_body['password'], 'utf-8'), gensalt()).decode('utf-8')
        del request_body['password']

        insert_one_result = db.admins.insert_one(request_body)
        inserted_id = str(insert_one_result.inserted_id)

        return CreateBookAdminOutput(
            201,
            'admin: %s created' % inserted_id,
            CreatedBookAdmin(inserted_id)
        )
