class InvalidRequestBodyError(Exception):
    pass


def is_book_admin_input_valid(request_body: dict):
    has_request_body_required_non_empty_string_field(request_body, 'email')
    has_request_body_required_non_empty_string_field(request_body, 'password')

    if 'firstName' in request_body:
        has_request_body_non_empty_string_field(request_body, 'firstName')
    if 'lastName' in request_body:
        has_request_body_non_empty_string_field(request_body, 'lastName')
    if 'version' in request_body:
        has_request_body_positive_integer_field(request_body, 'version')

    allowed_fields = ('email', 'password', 'firstName', 'lastName', 'version')
    for field in request_body.keys():
        if field not in allowed_fields:
            raise InvalidRequestBodyError('only keys: %s are allowed' % ', '.join(allowed_fields))


def is_book_admin_input_update_valid(request_body: dict):
    if 'email' in request_body:
        has_request_body_non_empty_string_field(request_body, 'email')
    if 'password' in request_body:
        has_request_body_non_empty_string_field(request_body, 'password')
    if 'firstName' in request_body:
        has_request_body_non_empty_string_field(request_body, 'firstName')
    if 'lastName' in request_body:
        has_request_body_non_empty_string_field(request_body, 'lastName')
    if 'version' in request_body:
        has_request_body_positive_integer_field(request_body, 'version')

    allowed_fields = ('email', 'password', 'firstName', 'lastName', 'version')
    for field in request_body.keys():
        if field not in allowed_fields:
            raise InvalidRequestBodyError('only keys: %s are allowed' % ', '.join(allowed_fields))


def has_request_body_required_non_empty_string_field(request_body: dict, field_name: str):
    if field_name not in request_body:
        raise InvalidRequestBodyError(field_name + ' key is required')
    has_request_body_non_empty_string_field(request_body, field_name)


def has_request_body_non_empty_string_field(request_body: dict, field_name: str):
    if not isinstance(request_body[field_name], str):
        raise InvalidRequestBodyError(field_name + ' must be string')
    if request_body[field_name] == '':
        raise InvalidRequestBodyError(field_name + ' must not be empty')
    if request_body[field_name].isspace():
        raise InvalidRequestBodyError(field_name + ' must not be blank')


def has_request_body_positive_integer_field(request_body: dict, field_name: str):
    if not isinstance(request_body[field_name], int):
        raise InvalidRequestBodyError(field_name + ' must be integer')
    if request_body[field_name] <= 0:
        raise InvalidRequestBodyError(field_name + ' must be positive integer')
