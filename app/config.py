from os import environ
from pathlib import Path

import yaml


def parse_config(path: str):
    with open(path) as f:
        try:
            return yaml.safe_load(f)
        except yaml.YAMLError as e:
            raise ('Reading config error', e)


config = parse_config(environ.get('CFG_FILE', default=Path(__file__).parent.parent.joinpath('secrets/local.env.yaml')))

APP_NAME = 'ex-book-admin-api-python-flask-restx'
RESTX_MASK_SWAGGER = False
MONGO_URI = 'mongodb://%s:%s@%s/%s?authSource=%s' % (
    config['mongodb']['user'],
    config['mongodb']['password'],
    config['mongodb']['host'],
    config['mongodb']['database'],
    config['mongodb']['authentication-database'])
