from typing import Dict

from flask_restx import Model
from flask_restx.fields import Integer, String, Nested


class CreatedBookAdmin:
    def __init__(self, id: str):
        self.id = id

    @staticmethod
    def get_model() -> Dict:
        return {
            'id': String(description='id of the created object')
        }


class CreateBookAdminOutput:
    def __init__(self, status: int, message: str, content: CreatedBookAdmin):
        self.status = status
        self.message = message
        self.content = content

    @staticmethod
    def get_model(created_book_admin: Model) -> Dict:
        return {
            'status': Integer(default=201),
            'message': String(),
            'content': Nested(created_book_admin)}
