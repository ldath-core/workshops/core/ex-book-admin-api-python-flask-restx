from typing import Dict

from flask_restx import Model
from flask_restx.fields import Integer, String, Nested


class BookAdminsOutput:
    def __init__(self, status: int, message: str, content: dict):
        self.status = status
        self.message = message
        self.content = content

    @staticmethod
    def get_model(book_admin_object: Model) -> Dict:
        return {
            'status': Integer(required=True, default=200, example=200),
            'message': String(required=True),
            'content': Nested(book_admin_object, required=True)}
