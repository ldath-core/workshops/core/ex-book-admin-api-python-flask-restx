from typing import Dict

from flask_restx.fields import String, Integer


class CreateBookAdminInput:
    @staticmethod
    def get_model() -> Dict:
        return {
            'email': String(required=True, nullable=False),
            'password': String(required=True, nullable=False),
            'firstName': String(nullable=False),
            'lastName': String(nullable=False),
            'version': Integer(nullable=False, default=1)}
