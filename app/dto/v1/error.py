from typing import Dict

from flask_restx.fields import Integer, String, List


class Error:
    def __init__(self, status: int, message: str, errors: list):
        self.status = status
        self.message = message
        self.errors = errors

    @staticmethod
    def get_model() -> Dict:
        return {
            'status': Integer(example=404),
            'message': String(example='book-list not found'),
            'errors': List(String, nullable=True)}
