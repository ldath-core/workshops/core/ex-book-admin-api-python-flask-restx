from typing import Dict

from flask_restx import Model
from flask_restx.fields import Integer, String, Nested, Boolean


class HealthContent:
    def __init__(self, alive: bool, mongo: bool):
        self.alive = alive
        self.mongo = mongo

    @staticmethod
    def get_model() -> Dict:
        return {
            'alive': Boolean(),
            'mongo': Boolean()}


class Health:
    def __init__(self, status: int, message: str, content: HealthContent):
        self.status = status
        self.message = message
        self.content = content

    @staticmethod
    def get_model(content: Model) -> Dict:
        return {
            'status': Integer(default=200),
            'message': String(default='book admin api health'),
            'content': Nested(content, nullable=True)}
