from typing import Dict

from flask_restx.fields import Integer, String


class BookAdminObject:
    @staticmethod
    def get_model() -> Dict:
        return {
            'id': String(required=True, readOnly=True, description='ID of the BookAdmin in the Database'),
            'email': String(required=True, nullable=False),
            'passwordHash': String(required=True, nullable=False),
            'firstName': String(required=True, nullable=False, default=''),
            'lastName': String(required=True, nullable=False, default=''),
            'version': Integer(required=True, nullable=False, default=1)}
